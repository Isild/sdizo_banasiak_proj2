#pragma once

/*
Autor: Przemysław Wujek 234983
Klasa listy dwukierunkowej zawierająca metody dodawania odejmowania elementów na następujących pozycjach:
- początek listy
- koniec listy
- dowolne miejsce w tablicy
Dodatkowo klasa zawiera głowę listy(wskaźnik na pierwszy element).
*/

#include "NodeTwoWayList.h"

class TwoWayList
{
public:
	//NodeTwoWayList *head;
	NodeTwoWayList * head;
	int size;

	void AddOnStart(int s, int e, int we);
	void AddOnEnd(int s, int e, int we);
	void AddInPlace(int s, int e, int we, int n);
	void DeleteStart();
	void DeleteEnd();
	void DeleteSelected(int n);
	void Show();

	TwoWayList();
	~TwoWayList();
};

