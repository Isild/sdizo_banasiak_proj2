#include "stdafx.h"
#include "BinSearchTree.h"


void BinSearchTree::SetRoot(BSTNode * r)
{
	root = r;
}

BSTNode * BinSearchTree::GetRoot()
{
	return root;
}

void BinSearchTree::AddToTree(int s, int e, int w)
{
	BSTNode *newNode = new BSTNode(s, e, w);

	if (!root)
	{
		root = newNode;
	}
	else
	{
		BSTNode *tmp = root;

		while (tmp)
		{
			if (tmp->weight <= newNode->weight)
			{
				if (tmp->right)
					tmp = tmp->right;
				else
				{
					newNode->up = tmp;
					tmp->right = newNode;
					break;
				}
			}
			else if (tmp->weight > newNode->weight)
			{
				if (tmp->left)
					tmp = tmp->left;
				else
				{
					newNode->up = tmp;
					tmp->left = newNode;
					break;
				}
			}
		}
	}
	size++;
}

void BinSearchTree::ShowBST(std::string sp, std::string sn, int v)
{
	std::string cr, cl, cp;
	cr = cl = cp = "  ";
	cr[0] = 218; cr[1] = 196;
	cl[0] = 192; cl[1] = 196;
	cp[0] = 179;
	std::string s;

	if (v < size)
	{
		s = sp;
		if (sn == cr) s[s.length() - 2] = ' ';
		ShowBST(s + cp, cr, 2 * v + 2);

		s = s.substr(0, sp.length() - 2);

		std::cout << s << sn << root[v].start << "-"<< root[v].end<<":"<< root[v].weight << std::endl;

		s = sp;
		if (sn == cl) s[s.length() - 2] = ' ';
		ShowBST(s + cp, cl, 2 * v + 1);
	}
}

void BinSearchTree::Show(BSTNode *tmp, int i)
{
	if (tmp)
	{
		if (tmp->right)
			Show((tmp->right), i++);
		if (tmp->left)
			Show((tmp->left), i++);
		std::cout << i << " " << tmp->start << "-" << tmp->end << ":" << tmp->weight << std::endl;
	}
}

List *BinSearchTree::InOrder(BSTNode *tmp)
{
	//BSTNode *tmp = root;
	//List *list = new List();

	//while (tmp)//szukam najmniejszego elementu
	//	if(tmp->left)
	//		tmp = tmp->left;
	if (tmp)
	{
		InOrder(tmp->left);
		list->Add(tmp->start, tmp->end, tmp->weight);
		InOrder(tmp->right);
	}
	//list->Add(tmp->start, tmp->end, tmp->weight);

	return list;
}

BinSearchTree::BinSearchTree()
{
	root = nullptr;
	size = 0;
}

BinSearchTree::BinSearchTree(BSTNode * r)
{
	root = r;
}


BinSearchTree::~BinSearchTree()
{
}
