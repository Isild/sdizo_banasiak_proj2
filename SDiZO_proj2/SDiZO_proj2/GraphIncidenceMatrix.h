#pragma once
class GraphIncidenceMatrix
{
private: 
	int N = 0;
	int M = 0;
	int **tabIncidence = nullptr;

public:

	int ReadMatrix(int w); //w-wierzcho�ek, nr wierzcho�ka
	void ReadOneTop(int vS, int vE, int w, int i);
	void ShowMatrix();
	void DijkstraLinear();//najkr�tsza droga
	void DijkstraLinear(int start, int end);
	void BellmanFord();
	int Kruskal();//drzewo rozpinaj�ce
	void Prima();

	void DijkstraLinearTime(int start, int end);
	void KruskalTime();//drzewo rozpinaj�ce

	

	int GetN();
	void SetN(int n);
	int GetM();
	void SetM(int m);
	int **GetTabIncidence();
	void SetTabIncidence(int **tab);
	void MatrixToFile();

	GraphIncidenceMatrix();
	GraphIncidenceMatrix(int **tab);
	GraphIncidenceMatrix(int n, int m);
	~GraphIncidenceMatrix();
};

