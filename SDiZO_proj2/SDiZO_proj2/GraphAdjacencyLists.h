#pragma once
#include "GraphTop.h"
#include <iostream>

class GraphAdjacencyLists
{
private:
	GraphTop **head;
	int N;
	int M;

public:
	void ReadList();
	void ReadOneElement(int vS, int vE, int w);
	void ShowList();
	void DijkstraLinear();
	void DijkstraLinear(int start);
	void DijkstraLinear(int start, int end);
	void BellmanFord();
	int Kruskal();
	void Prima();

	void DijkstraLinearTime(int start, int end);
	void KruskalTime();//drzewo rozpinające

	void SetHead(GraphTop **h);
	GraphTop **GetHead();
	void SetN(int n);
	int GetN();
	void SetM(int m);
	int GetM();

	GraphAdjacencyLists();
	GraphAdjacencyLists(GraphTop **h);
	GraphAdjacencyLists(int n, int m);
	GraphAdjacencyLists(GraphTop **h, int n, int m);
	~GraphAdjacencyLists();
};