#pragma once
#include "Node.h"
class BSTNode :
	public Node
{
public:
	BSTNode *up;
	BSTNode *left;
	BSTNode *right;

	BSTNode();
	BSTNode(int s, int e, int w);
	~BSTNode();
};