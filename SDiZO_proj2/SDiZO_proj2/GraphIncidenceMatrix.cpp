#include "stdafx.h"
#include "GraphIncidenceMatrix.h"
#include <iostream>
#include "BinSearchTree.h"
#include <fstream>
#include <cmath>


int GraphIncidenceMatrix::ReadMatrix(int w) //podaj pocz�tkowy wierzcho�ek i ko�cowy wierzcho�ek, je�eli zwr�ci 0 to b��dne wierzcho�ki
{
	int vP = -1, vK = -1, weight;
	std::cin >> vP;
	std::cin >> vK;
	std::cin >> weight;
	std::cout << std::endl;

	if (vP >= N || vK >= N || w >= M || vP == vK) //jak jest z vP == vK
		return 0;
	else
	{
		tabIncidence[vP][w] = 1 * weight;
		tabIncidence[vK][w] = -1 * weight;
		return 1;
	}
}

void GraphIncidenceMatrix::ReadOneTop(int vS, int vE, int w, int i)
{
	tabIncidence[vS][i] = 1 * w;
	tabIncidence[vE][i] = -1 * w;
}

void GraphIncidenceMatrix::ShowMatrix()
{
	printf("\n");
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < M; j++)
			printf("%3d ", tabIncidence[i][j]);
		printf("\n");
	}

	MatrixToFile();
}

void GraphIncidenceMatrix::DijkstraLinear()
{
	int *d = new int[N];
	int *p = new int[N];
	int *s = new int[N]; //stos
	int wskS = 0; //wska�nik stosu
	bool *QS = new bool[N]; //tablica m�wi�ca w kt�rym zbiorze jest wierzcho�ek 

	for (int i = 0; i < N; i++) //uzupe�nienie pocz�tkowe tablic pomocniczych 
	{
		d[i] = INT_MAX; //inf
		p[i] = -1;
		QS[i] = false;
	}
	d[0] = 0;

	//szukanie najkr�tszej drogi 
	for (int iP = 0; iP < N; iP++)
	{
		//szukam wierzcho�ka w d[] o najmniejszym ko�cie, kt�ry nie nale�y do S
		int min = d[0];
		for (int u = 0; u < N; u++)
		{
			if (!QS[u] && d[u] < INT_MAX)// je�li nie jest w S
			{
				min = u; // znajduje sobie jaki� wierzcho�ek kt�ry nie jest w s, �eby m�c znale�� mniejszego od niego je�li si� da
				break;
			}
		}

		for (int u = 0; u < N; u++)//w d szukam wierzcho�ka kt�ry nie jest w S o najmniejszym koszcie
		{
			if (d[u] < d[min] && !QS[u])
			{
				min = u;
			}
		}

		//std::cout << "najmniejszy wierzcholek: " << min << "\n";
		QS[min] = true;//znaleziony wierzcho�ek idzie do zbioru S

		for (int j = 0; j < M; j++)
		{
			if (tabIncidence[min][j] > 0)//szukam wierzcho�k�w kt�re s� po��czone z wierzcho�kiem min 
				for (int i = 0; i < N; i++)//szukam gdzie si� ko�czy droga
					if (tabIncidence[i][j] < 0)
					{
						if (d[i] > d[min] + tabIncidence[i][j] * (-1))
						{
							p[i] = min;
							d[i] = d[min] + tabIncidence[i][j] * (-1);
						}
						break;
					}
		}
		
	}
	/*		//wy�wietlenie kontrolne tablic d[] i p[]
	std::cout << "d[u]: ";
	for (int i = 0; i < N; i++)
		std::cout << d[i] << " ";
	std::cout << "\n";
	std::cout << "p[u]: ";
	for (int i = 0; i < N; i++)
		std::cout << p[i] << " ";
	std::cout << "\n\n";
	*/

	for (int i = 0; i < N; i++) //wy�wietlenie �cie�ki
	{
		std::cout << i << ": ";

		for (int j = i; j > -1; j = p[j]) 
			s[wskS++] = j;

		while (wskS) //�cie�ka wy�wietlana warto�ciami ze stosu od ko�ca
			std::cout << s[--wskS] << " ";

		std::cout << "Koszt: " << d[i] << std::endl;//koszt
	}

	delete[]d;
	delete[]p;
	delete[]s;
	delete[]QS;
}

void GraphIncidenceMatrix::DijkstraLinear(int start, int end)
{
	int *d = new int[N];
	int *p = new int[N];
	int *s = new int[N]; //stos
	int wskS = 0; //wska�nik stosu
	bool *QS = new bool[N]; //tablica m�wi�ca w kt�rym zbiorze jest wierzcho�ek 

	for (int i = 0; i < N; i++) //uzupe�nienie pocz�tkowe tablic pomocniczych 
	{
		d[i] = INT_MAX; //inf
		p[i] = -1;
		QS[i] = false;
	}
	d[start] = 0;

	//szukanie najkr�tszej drogi 
	for (int iP = 0; iP < N; iP++)
	{
		//szukam wierzcho�ka w d[] o najmniejszym ko�cie, kt�ry nie nale�y do S
		int min = start;											//d[start]
		for (int u = 0; u < N; u++)
		{
			if (!QS[u] && d[u] < INT_MAX)// je�li nie jest w S
			{
				min = u; // znajduje sobie jaki� wierzcho�ek kt�ry nie jest w s, �eby m�c znale�� mniejszego od niego je�li si� da
				break;
			}
		}

		for (int u = 0; u < N; u++)//w d szukam wierzcho�ka kt�ry nie jest w S o najmniejszym koszcie
		{
			if (d[u] < d[min] && !QS[u])
			{
				min = u;
			}
		}

		QS[min] = true;//znaleziony wierzcho�ek idzie do zbioru S

		for (int j = 0; j < M; j++)
		{
			if (tabIncidence[min][j] > 0)//szukam wierzcho�k�w kt�re s� po��czone z wierzcho�kiem min 
				for (int i = 0; i < N; i++)//szukam gdzie si� ko�czy droga
					if (tabIncidence[i][j] < 0)
					{
						if (d[i] > d[min] + tabIncidence[i][j] * (-1))
						{
							p[i] = min;
							d[i] = d[min] + tabIncidence[i][j] * (-1);
						}
						break;
					}
		}

	}
	/*		//wy�wietlenie kontrolne tablic d[] i p[]
	std::cout << "d[u]: ";
	for (int i = 0; i < N; i++)
	std::cout << d[i] << " ";
	std::cout << "\n";
	std::cout << "p[u]: ";
	for (int i = 0; i < N; i++)
	std::cout << p[i] << " ";
	std::cout << "\n\n";
	*/

	for (int i = 0; i < N; i++) //wy�wietlenie �cie�ki
	{
		if (i == end)
		{

			for (int j = i; j > -1; j = p[j])
				s[wskS++] = j;

			while (wskS) //�cie�ka wy�wietlana warto�ciami ze stosu od ko�ca
				std::cout << s[--wskS] << " ";

			if (d[i] != INT_MAX)
				std::cout << "Koszt: " << d[i] << std::endl;//koszt
			else
				std::cout << "Sciezka nie istnieje " << std::endl;
		}
	}

	delete[]d;
	delete[]p;
	delete[]s;
	delete[]QS;
}

void GraphIncidenceMatrix::BellmanFord()
{
	int *d = new int[N];
	int *p = new int[N];

	for (int i = 0; i < N; i++) //uzupe�nienie pocz�tkowe tablic pomocniczych 
	{
		d[i] = INT_MAX; //inf
		p[i] = -1;
	}
	d[0] = 0;

	std::cout << "BellmanFord lista." << std::endl;
}

int GraphIncidenceMatrix::Kruskal()
{
	//tworz� list� kraw�dzi L posortowan� wg wag
	BinSearchTree *head = new BinSearchTree(); //bst do przechowywania
	int il = 0; //zmienna pomocnicza do szukania pocz�tku i ko�ca
	int **tab = new int *[N]; //tablica pomocnicza do szukania cykli
	int sum = 0;

	for (int i = 0; i < N; i++)
	{
		tab[i] = new int [M];
	}

	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
			tab[i][j] = 0;

	for (int j = 0; j < M; j++)
	{
		int start = 0, end = 0, weight = 0;
		for (int i = 0; i < N; i++)
		{
			if (tabIncidence[i][j] != 0 && il==0)
			{
				start = i;
				il++;
			}
			else if (tabIncidence[i][j] != 0 && il == 1)
			{
				end = i;
				weight = abs(tabIncidence[i][j]);
				il++;
				break;
			}
		}
		il = 0;
		head->AddToTree(start, end, weight);
	}

	List *L = (head->InOrder((head->GetRoot()))); //uporz�dkowana lista L
	List *T = new List(); //lista drzewa rozpinaj�cego
	//tablica do wyznaczania kt�r� kraw�d� dodaje do listy
	int * tablica = new int[N];
	for (int i = 0; i < N; i++)
		tablica[i] = i;

	//L->ShowList();
	T->head; //dodajemy jeden element, �eby m�c do czego por�wnywa�
	if (tablica[L->head->start] != tablica[L->head->end]) //szukam algorytmem poznanym na �wiczeniach
	{
		int more = 0;

		if (tablica[L->head->start] < tablica[L->head->end])
		{
			more = tablica[L->head->end];

			for (int i = 0; i<N; i++)
				if (tablica[i] == more)
					tablica[i] = tablica[L->head->start];
		}
		else
		{
			more = tablica[L->head->start];

			for (int i = 0; i<N; i++)
				if (tablica[i] == more)
					tablica[i] = tablica[L->head->end];
		}
		T->Add(L->head->start, L->head->end, L->head->weight);
		sum += L->head->weight;
	}
	L->DeleteHead();
	T->head;

	while (L->head) //dodawanie do listy T tak aby nie by�o cykli
	//for (int j = 0; j < N - 1; j++)
	{

		/*std::cout << std::endl;
		std::cout << "T list" << std::endl;
		T->ShowList();
		std::cout << "L list" << std::endl;
		L->ShowList();
		std::cout << std::endl;
		//std::cout << std::endl;*/

		if (tablica[L->head->start] != tablica[L->head->end]) //szukam algorytmem poznanym na �wiczeniach
		{
			int more = 0;
			
			if (tablica[L->head->start] < tablica[L->head->end])
			{
				more = tablica[L->head->end];

				for(int i=0;i<N;i++)
					if(tablica[i] == more)
						tablica[i] = tablica[L->head->start];
			}
			else
			{
				more = tablica[L->head->start];

				for (int i = 0; i<N; i++)
					if (tablica[i] == more)
						tablica[i] = tablica[L->head->end];
			}
			T->Add(L->head->start, L->head->end, L->head->weight);
			sum += L->head->weight;
		}

		/*for (int i = 0; i < N; i++)
			std::cout << tablica[i] << " ";
		std::cout << "\n";*/
		
		L->DeleteHead();

	}

	L->ShowList();
	std::cout << std::endl;
	T->ShowList();
	return sum;
}


void GraphIncidenceMatrix::Prima()
{
	std::cout << "Prima lista." << std::endl;
}

void GraphIncidenceMatrix::DijkstraLinearTime(int start, int end)
{
	int *d = new int[N];
	int *p = new int[N];
	int *s = new int[N]; //stos
	int wskS = 0; //wska�nik stosu
	bool *QS = new bool[N]; //tablica m�wi�ca w kt�rym zbiorze jest wierzcho�ek 

	for (int i = 0; i < N; i++) //uzupe�nienie pocz�tkowe tablic pomocniczych 
	{
		d[i] = INT_MAX; //inf
		p[i] = -1;
		QS[i] = false;
	}
	d[start] = 0;

	//szukanie najkr�tszej drogi 
	for (int iP = 0; iP < N; iP++)
	{
		//szukam wierzcho�ka w d[] o najmniejszym ko�cie, kt�ry nie nale�y do S
		int min = d[start];											//d[start]
		for (int u = 0; u < N; u++)
		{
			if (!QS[u] && d[u] < INT_MAX)// je�li nie jest w S
			{
				min = u; // znajduje sobie jaki� wierzcho�ek kt�ry nie jest w s, �eby m�c znale�� mniejszego od niego je�li si� da
				break;
			}
		}

		for (int u = 0; u < N; u++)//w d szukam wierzcho�ka kt�ry nie jest w S o najmniejszym koszcie
		{
			if (d[u] < d[min] && !QS[u])
			{
				min = u;
			}
		}

		QS[min] = true;//znaleziony wierzcho�ek idzie do zbioru S

		for (int j = 0; j < M; j++)
		{
			if (tabIncidence[min][j] > 0)//szukam wierzcho�k�w kt�re s� po��czone z wierzcho�kiem min 
				for (int i = 0; i < N; i++)//szukam gdzie si� ko�czy droga
					if (tabIncidence[i][j] < 0)
					{
						if (d[i] > d[min] + tabIncidence[i][j] * (-1))
						{
							p[i] = min;
							d[i] = d[min] + tabIncidence[i][j] * (-1);
						}
						break;
					}
		}

	}
	/*		//wy�wietlenie kontrolne tablic d[] i p[]
	std::cout << "d[u]: ";
	for (int i = 0; i < N; i++)
	std::cout << d[i] << " ";
	std::cout << "\n";
	std::cout << "p[u]: ";
	for (int i = 0; i < N; i++)
	std::cout << p[i] << " ";
	std::cout << "\n\n";
	*/

	delete[]d;
	delete[]p;
	delete[]s;
	delete[]QS;
}

void GraphIncidenceMatrix::KruskalTime()
{
	//tworz� list� kraw�dzi L posortowan� wg wag
	BinSearchTree *head = new BinSearchTree(); //bst do przechowywania

	int il = 0; //zmienna pomocnicza do szukania pocz�tku i ko�ca

	for (int j = 0; j < M; j++)
	{
		int start = 0, end = 0, weight = 0;
		for (int i = 0; i < N; i++)
		{
			if (tabIncidence[i][j] != 0 && il == 0)
			{
				start = i;
				il++;
			}
			else if (tabIncidence[i][j] != 0 && il == 1)
			{
				end = i;
				weight = abs(tabIncidence[i][j]);
				il++;
				break;
			}
		}
		il = 0;
		//std::cout << "wierzcholek: " << start << "-" << end << ":" << weight << "\n";
		head->AddToTree(start, end, weight);
	}
	//std::cout << std::endl;
	//head->Show((head->GetRoot()));
	//head->ShowBST();

	List *L = (head->InOrder((head->GetRoot()))); //uporz�dkowana lista L
	List *T = new List(); //lista drzewa rozpinaj�cego

						  //L->ShowList();
	T->head; //dodajemy jeden element, �eby m�c do czego por�wnywa�
	T->Add(L->head->start, L->head->end, L->head->weight);
	L->DeleteHead();
	T->head;

	while (L->head) //dodawanie do listy T tak aby nie by�o cykli
	{
		bool isStart = false;
		bool isEnd = false;

		NodeList *tmp = T->head;
		while (tmp)
		{
			if (L->head->start == tmp->end)
				isStart = true;
			if (L->head->start == tmp->start)
				isStart = true;
			if (L->head->end == tmp->end)
				isEnd = true;
			if (L->head->end == tmp->start)
				isEnd = true;

			//if (tmp->next)
			tmp = tmp->next;
		}
		/*std::cout << std::endl;
		std::cout << "T list" << std::endl;
		T->ShowList();
		std::cout << "L list" << std::endl;
		L->ShowList();
		std::cout << std::endl;*/
		//std::cout << std::endl;

		if (isStart == true && isEnd == true)
			L->DeleteHead();
		else
		{
			T->Add(L->head->start, L->head->end, L->head->weight);
			L->DeleteHead();
		}
	}

	//L->ShowList();
	//std::cout << std::endl;
	//T->ShowList();
}

int GraphIncidenceMatrix::GetN()
{
	return N;
}

void GraphIncidenceMatrix::SetN(int n)
{
	N = n;
}

int GraphIncidenceMatrix::GetM()
{
	return M;
}

void GraphIncidenceMatrix::SetM(int m)
{
	M = m;
}

int ** GraphIncidenceMatrix::GetTabIncidence()
{
	return tabIncidence;
}

void GraphIncidenceMatrix::SetTabIncidence(int ** tab)
{
	tabIncidence = tab;
}

void GraphIncidenceMatrix::MatrixToFile()
{
	std::ofstream plik;
	std::string name;
	float d = 200 * M / (N*(N - 1));
	name = std::to_string((int)ceil(d)) + "_" + std::to_string(N); //nazwa pliku g�sto��_ilo�� wierzcho�k�w

	plik.open((name + "Macierz.txt"), std::ios::out);

	if (plik.good() == true)
	{
		for (int i = 0; i < N; i++)
		{
			for (int j = 0; j < M; j++)
			{
				plik << tabIncidence[i][j];
				plik << ", ";
			}
			plik << "\n";
		}
		
		plik.close();
	}

}

GraphIncidenceMatrix::GraphIncidenceMatrix()
{
	tabIncidence = nullptr;
	N = NULL;
	M = NULL;
}

GraphIncidenceMatrix::GraphIncidenceMatrix(int ** tab)
{
	tabIncidence = tab;
}

GraphIncidenceMatrix::GraphIncidenceMatrix(int n, int m) //tworz� macierz n-> na m |
{
	N = n;
	M = m;

	int **tab= new int *[N]; //najpierw tworz� tab n elementow�

	for (int i = 0; i < N; i++)//potem w ka�dym el tworz� kolejne M element�w
		tab[i] = new int[M];

	for (int i = 0; i < N; i++)//wype�niam zerami tablic�
		for (int j = 0; j < M; j++)
			tab[i][j] = 0;

	tabIncidence = tab;
}

GraphIncidenceMatrix::~GraphIncidenceMatrix()
{
	for (int i = 0; i < N; i++)
		delete[] tabIncidence[i];
	delete[] tabIncidence;
}
