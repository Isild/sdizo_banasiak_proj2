#pragma once

/*
Potrzebne funkcje do zliczenia czasu.
Zaporzyczone ze strony:
https://stackoverflow.com/questions/1739259/how-to-use-queryperformancecounter
*/

class TimeStoper
{
public:
	double PCFreq = 0.0;
	__int64 CounterStart = 0;

	void StartCounter();
	double GetCounter();

	TimeStoper();
	~TimeStoper();
};

