#include "stdafx.h"
#include "TimeStoper.h"
#include <windows.h>
#include <iostream>

using namespace std;

void TimeStoper::StartCounter()
{
	LARGE_INTEGER li;
	if (!QueryPerformanceFrequency(&li))
		cout << "QueryPerformanceFrequency failed!\n";

	PCFreq = double(li.QuadPart) / 1000.0;

	QueryPerformanceCounter(&li);
	CounterStart = li.QuadPart;
}

double TimeStoper::GetCounter()
{
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	return double(li.QuadPart - CounterStart) / PCFreq;
}

TimeStoper::TimeStoper()
{
}

TimeStoper::~TimeStoper()
{
}
