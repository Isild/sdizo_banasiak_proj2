#include "stdafx.h"
#include "BSTNode.h"


BSTNode::BSTNode()
{
}

BSTNode::BSTNode(int s, int e, int w) : Node(s,e,w)
{
	up = nullptr;
	left = nullptr;
	right = nullptr;
}


BSTNode::~BSTNode()
{
}
