#include "stdafx.h"
#include "TwoWayList.h"
#include "NodeTwoWayList.h"
#include <iostream>


void TwoWayList::AddOnStart(int s, int e, int we)
{
	if (head == nullptr)//je�li nie ma �adnego el w li�cie
	{
		//NodeTwoWayList *w = new NodeTwoWayList();
		NodeTwoWayList *w = new NodeTwoWayList(s, e, we);

		head = w;
	}
	else
	{
		NodeTwoWayList *w = new NodeTwoWayList(s,e,we);

		w->priev = nullptr; //mo�na zakomentowa�
		w->next = head;
		head->priev = w;//ustawienie dla obecnego heda nowej g�owy

		head = w;
	}
	size++;
}

void TwoWayList::AddOnEnd(int s, int e, int we)
{
	if (head == nullptr)
	{
		NodeTwoWayList *w = new NodeTwoWayList(s,e,we);

		head = w;
	}
	else
	{
		NodeTwoWayList *w = new NodeTwoWayList(s,e,we);
		NodeTwoWayList *tmp = head;

		while (tmp->next) //przechodz� do ostatniego elementu
			tmp = tmp->next;

		tmp->next = w;
		w->priev = tmp;
	}
	size++;
}

void TwoWayList::AddInPlace(int s, int e, int we, int n)//wstawiam do n'tego elementu listy
{
	if (n >= 0 && n <= size)
	{
		if (n == 0)
		{
			NodeTwoWayList *newElement = new NodeTwoWayList(s,e,we);

			newElement->next = head;
			head->priev = newElement;

			head = newElement;
		}
		else if (n == size)
		{
			NodeTwoWayList *tmp = head;
			NodeTwoWayList *newElement = new NodeTwoWayList(s,e,we);

			for (int i = 0; i < n - 1; i++)
				tmp = tmp->next;

			tmp->next = newElement;
			newElement->priev = tmp;
		}
		else
		{
			NodeTwoWayList *tmp = head;
			NodeTwoWayList *newElement = new NodeTwoWayList(s,e,we);

			for (int i = 0; i < n; i++)
				tmp = tmp->next;

			tmp->priev->next = newElement; //w�o�enie pomi�dzy dwa w�z�y nowego elementu
			newElement->priev = tmp->priev;
			tmp->priev = newElement;
			newElement->next = tmp;
		}
		size++;
	}
	else
		std::cout << "Nie mo�na doda� elementu na ten indeks." << std::endl;
}

void TwoWayList::DeleteStart()
{
	if (size)
	{
		if (size == 1)
		{
			head->start = NULL;
			head->end = NULL;
			head->weight = NULL;
			head = nullptr;

			delete head;
		}
		else
		{
			NodeTwoWayList *tmp = head->next;

			tmp->priev = nullptr;

			head->next = nullptr;
			head->priev = nullptr;
			head->start = NULL;
			head->end = NULL;
			head->weight = NULL;
			delete head;

			head = tmp;
		}
		size--;
	}
}

void TwoWayList::DeleteEnd()
{
	if (size)
	{
		if (size == 1)
		{
			head->start = NULL;
			head->end = NULL;
			head->weight = NULL;
			head = nullptr;

			delete head;
		}
		else
		{
			NodeTwoWayList *tmp = head;

			while (tmp->next)
				tmp = tmp->next;

			tmp->priev->next = nullptr;//ustawienie przedostatniego elementu jako ostatni
			tmp->priev = nullptr;
			tmp->start = NULL;
			tmp->end = NULL;
			tmp->weight = NULL;

			delete tmp;
		}
		size--;
	}
}

void TwoWayList::DeleteSelected(int n)
{
	if (size && n >= 0 && n < size)
	{
		if (size == 1)
		{
			head->start = NULL;
			head->end = NULL;
			head->weight = NULL;
			head = nullptr;

			delete head;
		}
		else
		{
			NodeTwoWayList *tmp = head;

			for (int i = 0; i < n; i++) //przej�cie do elementu do konkretnego elementu, indeksujemy od 1
				tmp = tmp->next;

			if (tmp->priev == nullptr)//dla el pierwszego(g�owy)
			{
				head = tmp->next;
				tmp->next->priev = nullptr;
			}
			else
			{
				if (tmp->next == nullptr)//dla el ostatniego
				{
					tmp->priev->next = nullptr;
				}
				else //reszta przypadk�w
				{
					tmp->priev->next = tmp->next;
					tmp->next->priev = tmp->priev;
				}
			}

			tmp->next = nullptr;
			tmp->priev = nullptr;
			tmp->start = NULL;
			tmp->end = NULL;
			tmp->weight = NULL;
			delete tmp;
		}
		size--;
	}
	else
		std::cout << "Podano zly indeks." << std::endl;
}

void TwoWayList::Show()
{
	NodeTwoWayList *tmp = head;

	for (int i = 0; i < size; i++)
	{
		std::cout << tmp->start << "-" << tmp->end << ":" << tmp->weight << " ";
		tmp = tmp->next;
	}
}

TwoWayList::TwoWayList()
{
	size = 0;
}

TwoWayList::~TwoWayList()
{
	while (size)
	{
		DeleteEnd();
	}
}
