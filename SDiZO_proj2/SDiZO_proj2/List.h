#pragma once

#include "NodeList.h"

class List
{
public:
	NodeList *head;

	void Add(int s, int e, int w);
	void ShowList();
	void DeleteHead();

	List();
	~List();
};

