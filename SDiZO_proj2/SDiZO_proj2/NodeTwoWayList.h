#pragma once
#include "Node.h"
class NodeTwoWayList :
	public Node
{
public:
	NodeTwoWayList * next = nullptr;
	NodeTwoWayList *priev = nullptr;

	NodeTwoWayList();
	NodeTwoWayList(int s, int e, int w);
	~NodeTwoWayList();
};

