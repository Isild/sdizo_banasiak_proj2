#pragma once
class GraphTop
{
private:
	GraphTop *next = nullptr;
	int value;
	int weight;
public:
	GraphTop *GetGraphTopNext();
	void SetGraphTopNext(GraphTop* n);
	int GetValue();
	void SetValue(int v);
	int GetWeight();
	void SetWeight(int w);

	GraphTop(); 
	GraphTop(int v, int w);
	GraphTop(int v, int w, GraphTop *n);
	~GraphTop();
};

