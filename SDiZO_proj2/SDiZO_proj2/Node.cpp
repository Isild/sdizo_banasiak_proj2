#include "stdafx.h"
#include "Node.h"


Node::Node()
{
}


Node::Node(int s, int e, int w)
{
	start = s;
	end = e;
	weight = w;
}

Node::~Node()
{
	start = NULL;
	end = NULL;
	weight = NULL;
}
