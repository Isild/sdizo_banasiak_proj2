#include "stdafx.h"
#include "GraphAdjacencyLists.h"
#include "BinSearchTree.h"
#include <iostream>
#include "List.h"

void GraphAdjacencyLists::ReadList()
{
	int vS, vE, w;

	for (int i = 0; i < M; i++)
	{
		GraphTop *tmp = new GraphTop();

		std::cin >> vS; //wczytanie pocz�tku, ko�ca, wagi po��czenia
		std::cin >> vE;
		std::cin >> w;
		std::cout << std::endl;

		tmp->SetWeight(w);
		tmp->SetValue(vE);
		tmp->SetGraphTopNext(head[vS]); //element jest dodawany na pocz�tek listy
		head[vS] = tmp;
	}
}

void GraphAdjacencyLists::ReadOneElement(int vS, int vE, int w)
{
	GraphTop *tmp = new GraphTop();

	tmp->SetWeight(w);
	tmp->SetValue(vE);
	tmp->SetGraphTopNext(head[vS]); //element jest dodawany na pocz�tek listy
	head[vS] = tmp;
}

void GraphAdjacencyLists::ShowList()
{
	GraphTop *tmp;

	for (int i = 0; i < N; i++)
	{
		std::cout << "Lista[" << i << "] = ";
		tmp = head[i];
		
		while (tmp)
		{
			std::cout << tmp->GetValue() << ":" << tmp->GetWeight() << " ";
			tmp = tmp->GetGraphTopNext();
		}
		std::cout << std::endl;
	}
}

void GraphAdjacencyLists::DijkstraLinear()
{
	int *d = new int[N];
	int *p = new int[N];
	int *s = new int[N]; //stos
	int wskS = 0; //wska�nik stosu
	bool *QS = new bool[N]; //tablica m�wi�ca w kt�rym zbiorze jest wierzcho�ek 

	for (int i = 0; i < N; i++) //uzupe�nienie pocz�tkowe tablic pomocniczych 
	{
		d[i] = INT_MAX; //inf
		p[i] = -1;
		QS[i] = false;
	}
	d[0] = 0;

	//szukanie najkr�tszej drogi 
	for (int iP = 0; iP < N; iP++)
	{
		//szukam wierzcho�ka w d[] o najmniejszym ko�cie, kt�ry nie nale�y do S
		int min = d[0];
		for (int u = 0; u < N; u++)
		{
			if (!QS[u] && d[u] < INT_MAX)// je�li nie jest w S
			{
				min = u; // znajduje sobie jaki� wierzcho�ek kt�ry nie jest w s, �eby m�c znale�� mniejszego od niego je�li si� da
				break;
			}
		}

		for (int u = 0; u < N; u++)//w d szukam wierzcho�ka kt�ry nie jest w S o najmniejszym koszcie
		{
			if (d[u] < d[min] && !QS[u])
			{
				min = u;
			}
		}

		//std::cout << "najmniejszy wierzcholek: " << min << "\n";
		QS[min] = true;//znaleziony wierzcho�ek idzie do zbioru S

		GraphTop *tmp = head[min];
		while (tmp)
		{
			if (d[tmp->GetValue()] > d[min] + tmp->GetWeight())
			{
				p[tmp->GetValue()] = min;
				d[tmp->GetValue()] = d[min] + tmp->GetWeight();
			}


			tmp = tmp->GetGraphTopNext();
		}
	}

	for (int i = 0; i < N; i++) //wy�wietlenie �cie�ki
	{
		std::cout << i << ": ";

		for (int j = i; j > -1; j = p[j])
			s[wskS++] = j;

		while (wskS) //�cie�ka wy�wietlana warto�ciami ze stosu od ko�ca
			std::cout << s[--wskS] << " ";

		std::cout << "Koszt: " << d[i] << std::endl;//koszt
	}

	delete[]d;
	delete[]p;
	delete[]s;
	delete[]QS;
}

void GraphAdjacencyLists::DijkstraLinear(int start)
{
	int *d = new int[N];
	int *p = new int[N];
	int *s = new int[N]; //stos
	int wskS = 0; //wska�nik stosu
	bool *QS = new bool[N]; //tablica m�wi�ca w kt�rym zbiorze jest wierzcho�ek 

	for (int i = 0; i < N; i++) //uzupe�nienie pocz�tkowe tablic pomocniczych 
	{
		d[i] = INT_MAX; //inf
		p[i] = -1;
		QS[i] = false;
	}
	d[start] = 0;

	//szukanie najkr�tszej drogi 
	for (int iP = 0; iP < N; iP++)
	{
		//szukam wierzcho�ka w d[] o najmniejszym ko�cie, kt�ry nie nale�y do S
		int min = d[start];
		for (int u = 0; u < N; u++)
		{
			if (!QS[u] && d[u] < INT_MAX)// je�li nie jest w S
			{
				min = u; // znajduje sobie jaki� wierzcho�ek kt�ry nie jest w s, �eby m�c znale�� mniejszego od niego je�li si� da
				break;
			}
		}

		for (int u = 0; u < N; u++)//w d szukam wierzcho�ka kt�ry nie jest w S o najmniejszym koszcie
		{
			if (d[u] < d[min] && !QS[u])
			{
				min = u;
			}
		}

		//std::cout << "najmniejszy wierzcholek: " << min << "\n";
		QS[min] = true;//znaleziony wierzcho�ek idzie do zbioru S

		GraphTop *tmp = head[min];
		while (tmp)
		{
			if (d[tmp->GetValue()] > d[min] + tmp->GetWeight())
			{
				p[tmp->GetValue()] = min;
				d[tmp->GetValue()] = d[min] + tmp->GetWeight();
			}
			tmp = tmp->GetGraphTopNext();
		}
	}

	for (int i = 0; i < N; i++) //wy�wietlenie �cie�ki
	{
		std::cout << i << ": ";

		for (int j = i; j > -1; j = p[j])
			s[wskS++] = j;

		while (wskS) //�cie�ka wy�wietlana warto�ciami ze stosu od ko�ca
			std::cout << s[--wskS] << " ";

		std::cout << "Koszt: " << d[i] << std::endl;//koszt
	}

	delete[]d;
	delete[]p;
	delete[]s;
	delete[]QS;
}

void GraphAdjacencyLists::DijkstraLinear(int start, int end)
{
	int *d = new int[N];
	int *p = new int[N];
	int *s = new int[N]; //stos
	int wskS = 0; //wska�nik stosu
	bool *QS = new bool[N]; //tablica m�wi�ca w kt�rym zbiorze jest wierzcho�ek 

	for (int i = 0; i < N; i++) //uzupe�nienie pocz�tkowe tablic pomocniczych 
	{
		d[i] = INT_MAX; //inf
		p[i] = -1;
		QS[i] = false;
	}
	d[start] = 0;

	//szukanie najkr�tszej drogi 
	for (int iP = 0; iP < N; iP++)
	{
		//szukam wierzcho�ka w d[] o najmniejszym ko�cie, kt�ry nie nale�y do S
		int min = start;//
		for (int u = 0; u < N; u++)
		{
			if (!QS[u] && d[u] < INT_MAX)// je�li nie jest w S
			{
				min = u; // znajduje sobie jaki� wierzcho�ek kt�ry nie jest w s, �eby m�c znale�� mniejszego od niego je�li si� da
				break;
			}
		}

		for (int u = 0; u < N; u++)//w d szukam wierzcho�ka kt�ry nie jest w S o najmniejszym koszcie
		{
			if (d[u] < d[min] && !QS[u])
			{
				min = u;
			}
		}

		//std::cout << "najmniejszy wierzcholek: " << min << "\n";
		QS[min] = true;//znaleziony wierzcho�ek idzie do zbioru S

		GraphTop *tmp = head[min];
		while (tmp)
		{
			if (d[tmp->GetValue()] > d[min] + tmp->GetWeight())
			{
				p[tmp->GetValue()] = min;
				d[tmp->GetValue()] = d[min] + tmp->GetWeight();
			}
			tmp = tmp->GetGraphTopNext();
		}
	}
	
	for (int i = 0; i < N; i++) //wy�wietlenie �cie�ki
	{
		if (i == end)
		{
			for (int j = i; j > -1; j = p[j])
				s[wskS++] = j;

			while (wskS) //�cie�ka wy�wietlana warto�ciami ze stosu od ko�ca
				std::cout << s[--wskS] << " ";

			if(d[i] != INT_MAX)
				std::cout << "Koszt: " << d[i] << std::endl;//koszt
			else 
				std::cout << "Sciezka nie istnieje " << std::endl;
		}
	}

	delete[]d;
	delete[]p;
	delete[]s;
	delete[]QS;
}

void GraphAdjacencyLists::BellmanFord()
{
	std::cout << "BellmanFord lista." << std::endl;
}

int GraphAdjacencyLists::Kruskal()
{
	//tworz� list� kraw�dzi L posortowan� wg wag
	BinSearchTree *root = new BinSearchTree();//bst do przechowywania
	int il = 0; //zmienna pomocnicza do szukania pocz�tku i ko�ca
	GraphTop *tmp;
	int sum = 0;

	for (int i = 0; i < N; i++)
	{
		tmp = head[i];

		while (tmp)
		{
			root->AddToTree(i, tmp->GetValue(), tmp->GetWeight());
			tmp = tmp->GetGraphTopNext();
		}
	}

	List *L = (root->InOrder((root->GetRoot()))); //uporz�dkowana lista L
	List *T = new List(); //lista drzewa rozpinaj�cego
	//tablica do wyznaczania kt�r� kraw�d� dodaje do listy
	int * tablica = new int[N];
	for (int i = 0; i < N; i++)
		tablica[i] = i;
	//L->ShowList();

	T->head; //dodajemy jeden element, �eby m�c do czego por�wnywa�
	if (tablica[L->head->start] != tablica[L->head->end]) //szukam algorytmem poznanym na �wiczeniach
	{
		int more = 0;

		if (tablica[L->head->start] < tablica[L->head->end])
		{
			more = tablica[L->head->end];

			for (int i = 0; i<N; i++)
				if (tablica[i] == more)
					tablica[i] = tablica[L->head->start];
		}
		else
		{
			more = tablica[L->head->start];

			for (int i = 0; i<N; i++)
				if (tablica[i] == more)
					tablica[i] = tablica[L->head->end];
		}
		T->Add(L->head->start, L->head->end, L->head->weight);
		sum += L->head->weight;
	}
	L->DeleteHead();
	T->head;



	while (L->head) //dodawanie do listy T tak aby nie by�o cykli
	{
		if (tablica[L->head->start] != tablica[L->head->end]) //szukam algorytmem poznanym na �wiczeniach
		{
			int more = 0;

			if (tablica[L->head->start] < tablica[L->head->end])
			{
				more = tablica[L->head->end];

				for (int i = 0; i<N; i++)
					if (tablica[i] == more)
						tablica[i] = tablica[L->head->start];
			}
			else
			{
				more = tablica[L->head->start];

				for (int i = 0; i<N; i++)
					if (tablica[i] == more)
						tablica[i] = tablica[L->head->end];
			}
			T->Add(L->head->start, L->head->end, L->head->weight);
			sum += L->head->weight;
		}

		/*for (int i = 0; i < N; i++)
		std::cout << tablica[i] << " ";
		std::cout << "\n";*/
		
		L->DeleteHead();
	}

	//L->ShowList();
	std::cout << std::endl;
	T->ShowList();
	return sum;
}

void GraphAdjacencyLists::Prima()
{
	std::cout << "Prima lista." << std::endl;
}

void GraphAdjacencyLists::DijkstraLinearTime(int start, int end)
{

	int *d = new int[N];
	int *p = new int[N];
	int *s = new int[N]; //stos
	int wskS = 0; //wska�nik stosu
	bool *QS = new bool[N]; //tablica m�wi�ca w kt�rym zbiorze jest wierzcho�ek 

	for (int i = 0; i < N; i++) //uzupe�nienie pocz�tkowe tablic pomocniczych 
	{
		d[i] = INT_MAX; //inf
		p[i] = -1;
		QS[i] = false;
	}
	d[start] = 0;

	//szukanie najkr�tszej drogi 
	for (int iP = 0; iP < N; iP++)
	{
		//szukam wierzcho�ka w d[] o najmniejszym ko�cie, kt�ry nie nale�y do S
		int min = d[start];
		for (int u = 0; u < N; u++)
		{
			if (!QS[u] && d[u] < INT_MAX)// je�li nie jest w S
			{
				min = u; // znajduje sobie jaki� wierzcho�ek kt�ry nie jest w s, �eby m�c znale�� mniejszego od niego je�li si� da
				break;
			}
		}

		for (int u = 0; u < N; u++)//w d szukam wierzcho�ka kt�ry nie jest w S o najmniejszym koszcie
		{
			if (d[u] < d[min] && !QS[u])
			{
				min = u;
			}
		}

		QS[min] = true;//znaleziony wierzcho�ek idzie do zbioru S

		GraphTop *tmp = head[min];
		while (tmp)
		{
			if (d[tmp->GetValue()] > d[min] + tmp->GetWeight())
			{
				p[tmp->GetValue()] = min;
				d[tmp->GetValue()] = d[min] + tmp->GetWeight();
			}

			tmp = tmp->GetGraphTopNext();
		}
	}

	delete[]d;
	delete[]p;
	delete[]s;
	delete[]QS;
}

void GraphAdjacencyLists::KruskalTime()
{
	//tworz� list� kraw�dzi L posortowan� wg wag
	BinSearchTree *root = new BinSearchTree();//bst do przechowywania
	int il = 0; //zmienna pomocnicza do szukania pocz�tku i ko�ca
	GraphTop *tmp;

	for (int i = 0; i < N; i++)
	{
		tmp = head[i];

		while (tmp)
		{
			root->AddToTree(i, tmp->GetValue(), tmp->GetWeight());
			tmp = tmp->GetGraphTopNext();
		}
	}

	List *L = (root->InOrder((root->GetRoot()))); //uporz�dkowana lista L
	List *T = new List(); //lista drzewa rozpinaj�cego

						  //L->ShowList();

	T->head; //dodajemy jeden element, �eby m�c do czego por�wnywa�
	T->Add(L->head->start, L->head->end, L->head->weight);
	L->DeleteHead();
	T->head;

	while (L->head) //dodawanie do listy T tak aby nie by�o cykli
	{
		bool isStart = false;
		bool isEnd = false;

		NodeList *tmp = T->head;
		while (tmp)
		{
			if (L->head->start == tmp->end)
				isStart = true;
			if (L->head->start == tmp->start)
				isStart = true;
			if (L->head->end == tmp->end)
				isEnd = true;
			if (L->head->end == tmp->start)
				isEnd = true;

			//if (tmp->next)
			tmp = tmp->next;
		}
		if (isStart == true && isEnd == true)
			L->DeleteHead();
		else
		{
			T->Add(L->head->start, L->head->end, L->head->weight);
			L->DeleteHead();
		}
	}

	//L->ShowList();
	//std::cout << std::endl;
	//T->ShowList();
}

void GraphAdjacencyLists::SetHead(GraphTop ** h)
{
	head = h;
}

GraphTop ** GraphAdjacencyLists::GetHead()
{
	return head;
}

void GraphAdjacencyLists::SetN(int n)
{
	N = n;
}

int GraphAdjacencyLists::GetN()
{
	return N;
}

void GraphAdjacencyLists::SetM(int m)
{
	M = m;
}

int GraphAdjacencyLists::GetM()
{
	return M;
}

GraphAdjacencyLists::GraphAdjacencyLists()
{
	head = nullptr;
	M = NULL;
	N = NULL;
}

GraphAdjacencyLists::GraphAdjacencyLists(GraphTop ** h)
{
	head = h;
}

GraphAdjacencyLists::GraphAdjacencyLists(int n, int m)
{
	N = n;
	M = m;

	GraphTop **tab = new GraphTop *[N];
	head = tab;

	for (int i = 0; i < N; i++)
		head[i] = NULL;
}

GraphAdjacencyLists::GraphAdjacencyLists(GraphTop ** h, int n, int m)
{
	head = h;
	N = n;
	M = m;

	for (int i = 0; i < N; i++)
		head[i] = NULL;
}

GraphAdjacencyLists::~GraphAdjacencyLists()
{
	N = NULL;
	M = NULL;
	delete head;
	head = nullptr;
}
