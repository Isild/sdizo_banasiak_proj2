#include "stdafx.h"
#include "List.h"
#include <iostream>


void List::Add(int s, int e, int w)
{
	if (!head)
	{
		NodeList *newNode = new NodeList(s, e, w);
		head = newNode;
	}
	else
	{
		NodeList *tmp = head;

		while (tmp->next)
			tmp = tmp->next;

		NodeList *newNode = new NodeList(s, e, w);

		tmp->next = newNode;
	}
}

void List::ShowList()
{
	NodeList *tmp = head;

	while (tmp)
	{
		std::cout << tmp->start << "-" << tmp->end << ":" << tmp->weight << std::endl;
		tmp = tmp->next;
	}
}

void List::DeleteHead()
{
	NodeList *tmp = head;

	head = tmp->next;

	delete tmp;
}

List::List()
{
	head = nullptr;
}


List::~List()
{
}
