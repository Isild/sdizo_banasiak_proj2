#include "stdafx.h"
#include <iostream>
#include "menu.h"
#include <cstdlib>
#include <fstream>
#include <ctime>
#include <string>
#include "TimeStoper.h"
#include <sstream>
#include <cmath>

void menu()
{
	int odp = 0;
	int **t = nullptr;
	int x = 0;
	int q = 0;
	std::string name = "";

oneMoreTime:
	std::cout << "\n(1) Wczytaj dane z pliku.\n(2) Wygeneruj graf losowo.\n(3) Wyswietl graf listowo i macierzowo na ekranie." << std::endl;
	std::cout << "(4) Algorytm Kruskala macierzowo i listowo z wyswietleniem wynikow.\n(5) Algorytm Dijkstra macierzowo i listowo z wyswietleniem wynikow." << std::endl;
	std::cout << "(6) Algorytm Bellmana - Forda macierzowo i listowo z wyswietleniem wynikow.\n(7) Algorytm Prima macierzowo i listowo z wyswietleniem wynikow." << std::endl;
	std::cout << "(8) Test.\n(9) Wyjscie." << std::endl;
	std::cin >> odp;

	switch (odp)
	{
	case 1:
		std::cout << "Podaj nazwe pliku(program zapisuje/wczytuje dane do plikow \"gestosc_iloscWierzcholkow\", wprowadz ilsc wierzcholkow i gestosc grafu):";
		std::cin >> x;
		std::cin >> q;
		
		if (x == 0)
		{
			std::cout << "Brak wierzcholkow." << std::endl;
		}
		else if (q == 0)
		{
			std::cout << "Brak krawedzi." << std::endl;
		}
		else
		{
			//name = std::to_string(q) + "_" + std::to_string(x);
			name = std::to_string(x);
			t = ReadFromFile(name, q, x);
		}

		break;
	case 2:
		
		std::cout << "Podaj n - liczbe wierzcholkow grafu: ";
		std::cin >> x;
		std::cout << "q - gestosc grafu: ";
		std::cin >> q;

		t = GenerateGraph(q, x);
		break;
	case 3:
		if (t == nullptr)
		{
			std::cout << "Brak grafu. Wczytaj graf z pliku, lub wygeneruj losowy." << std::endl;
			break;
		}
		std::cout << "Tablicowo: " << std::endl;
		ShowGraphIncidence(t, x, q*x*(x - 1) / 200);
		std::cout << std::endl << "Lista sasiedztwa: " << std::endl;
		ShowGraphAdjacencyLists(t, x, q*x*(x - 1) / 200);
		break;
	case 4:
		if (t == nullptr)
		{
			std::cout << "Brak grafu. Wczytaj lub wygeneruj graf." << std::endl;
		}
		else if (q == 0)
		{
			std::cout << "Brak krawedzi." << std::endl;
		}
		else if (x == 0)
		{
			std::cout << "Brak wierzcholkow." << std::endl;
		}
		else
		{
			std::cout << "Czas Kruskala macierzowo: " << KruskalGraphIncidence(t, x, q*x*(x - 1) / 200) << "\n" << std::endl;
			std::cout << "Czas Kruskala listwow: " << KruskalGraphAdjacencyLists(t, x, q*x*(x - 1) / 200) << std::endl;
		}
		break;
	case 5:
		if (t == nullptr)
		{
			std::cout << "Brak grafu. Wczytaj lub wygeneruj graf." << std::endl;
		}
		else if (q == 0)
		{
			std::cout << "Brak krawedzi." << std::endl;
		}
		else if (x == 0)
		{
			std::cout << "Brak wierzcholkow." << std::endl;
		}
		else
		{
			int start=0, end=0;
			std::cout << "Podaj poczatek:";
			std::cin >> start;
			std::cout << "Podaj koniec:";
			std::cin >> end;

			std::cout << "Czas Dijkstry macierzowo: " << DijkstrGraphIncidence(t, x, q*x*(x - 1) / 200, start, end) << "\n" << std::endl;
			std::cout << "Czas Dijkstry listowo: " << DijkstrGraphAdjacencyLists(t, x, q*x*(x - 1) / 200, start, end) << std::endl;
		}
		break;
	case 6:
		if (t == nullptr)
		{
			std::cout << "Brak grafu. Wczytaj lub wygeneruj graf." << std::endl;
		}
		else if (q == 0)
		{
			std::cout << "Brak krawedzi." << std::endl;
		}
		else if (x == 0)
		{
			std::cout << "Brak wierzcholkow." << std::endl;
		}
		else
		{
			int start = 0, end = 0;
			std::cout << "Podaj poczatek:";
			std::cin >> start;
			std::cout << "Podaj koniec:";
			std::cin >> end;
			std::cout << "Czas Bellmana-Forda macierzowo: " << BellmanaFordaGraphIncidence(t, x, q*x*(x - 1) / 200, start, end) << "\n" << std::endl;
			std::cout << "Czas Bellmana-Forda listowo: " << BellmanaFordaGraphAdjacencyLists(t, x, q*x*(x - 1) / 200, start, end) << std::endl;
		}
		break;
	case 7:
		if (t == nullptr)
		{
			std::cout << "Brak grafu. Wczytaj lub wygeneruj graf." << std::endl;
		}
		else if (q == 0)
		{
			std::cout << "Brak krawedzi." << std::endl;
		}
		else if (x == 0)
		{
			std::cout << "Brak wierzcholkow." << std::endl;
		}
		else
		{
			std::cout << "Czas Prima macierzowo: " << PrimaGraphIncidence(t, x, q*x*(x - 1) / 200) << "\n" << std::endl;
			std::cout << "Czas Prima listowo: " << PrimaGraphAdjacencyLists(t, x, q*x*(x - 1) / 200) << std::endl;
		}
		break;
	case 8:
		if (x != 0 && q != 0)
		{
			Test(t, x, q*x*(x - 1) / 200);
		}
		else
			std::cout << "Zly graf." << std::endl;
		
		break;
	case 9:
		goto exit;
		break;
	default:
		std::cout << "Nie ma takiej opcji, sprobuj jeszcze raz." << std::endl;
		break;
	}
	std::cout << std::endl;
	system("PAUSE");
	goto oneMoreTime;
exit:
	std::cout << " ";
}

void TestGraphIncidence(int **t, int topNumber, int edgeNumber)
{
	GraphIncidenceMatrix macierz(topNumber, edgeNumber);

	for (int i = 0; i < edgeNumber; i++)
		macierz.ReadOneTop(t[i][0], t[i][1], t[i][2], i);
	
	macierz.ShowMatrix();
	std::cout << std::endl;
	
	std::cout << "Dijkstra: " << std::endl;
	macierz.DijkstraLinear();
	std::cout << "Kruskal: " << std::endl;
	macierz.Kruskal();
}

void ShowGraphIncidence(int ** t, int topNumber, int edgeNumber)
{
	GraphIncidenceMatrix macierz(topNumber, edgeNumber);

	for (int i = 0; i < edgeNumber; i++)
		macierz.ReadOneTop(t[i][0], t[i][1], t[i][2], i);

	macierz.ShowMatrix();
}

double KruskalGraphIncidence(int ** t, int topNumber, int edgeNumber)
{
	GraphIncidenceMatrix macierz(topNumber, edgeNumber);
	TimeStoper stoper;
	double returnStoper = 0;
	int wynik = 0;

	for (int i = 0; i < edgeNumber; i++)
		macierz.ReadOneTop(t[i][0], t[i][1], t[i][2], i);

	std::cout << "Kruskal Macierzowo: ";
	stoper.StartCounter();
	wynik = macierz.Kruskal();
	returnStoper = stoper.GetCounter();
	std::cout << wynik << "\n";

	return returnStoper;
}

double DijkstrGraphIncidence(int ** t, int topNumber, int edgeNumber, int start, int end)
{
	GraphIncidenceMatrix macierz(topNumber, edgeNumber);
	TimeStoper stoper;
	double returnStoper = -1;

	for (int i = 0; i < edgeNumber; i++)
		macierz.ReadOneTop(t[i][0], t[i][1], t[i][2], i);

	std::cout << "Dijkstra Macierzowo: \n";
	stoper.StartCounter();
	macierz.DijkstraLinear(start, end);
	returnStoper = stoper.GetCounter();

	return returnStoper;
}

double PrimaGraphIncidence(int ** t, int topNumber, int edgeNumber)
{
	GraphIncidenceMatrix macierz(topNumber, edgeNumber);
	TimeStoper stoper;
	double returnStoper = 0;

	for (int i = 0; i < edgeNumber; i++)
		macierz.ReadOneTop(t[i][0], t[i][1], t[i][2], i);

	std::cout << "Prima Macierzowo: \n";
	stoper.StartCounter();
	macierz.Prima();
	returnStoper = stoper.GetCounter();

	return returnStoper;
}

double BellmanaFordaGraphIncidence(int ** t, int topNumber, int edgeNumber, int start, int end)
{
	GraphIncidenceMatrix macierz(topNumber, edgeNumber);
	TimeStoper stoper;
	double returnStoper = -1;

	for (int i = 0; i < edgeNumber; i++)
		macierz.ReadOneTop(t[i][0], t[i][1], t[i][2], i);

	
		std::cout << "Bellman-Ford Macierzowo: \n";
		stoper.StartCounter();
		macierz.BellmanFord();
		returnStoper = stoper.GetCounter();
	

	return returnStoper;
}

void TestGraphAdjacencyLists(int **t, int topNumber, int edgeNumber)
{
	GraphAdjacencyLists macierz(topNumber, edgeNumber);

	for (int i = 0; i < edgeNumber; i++)
		macierz.ReadOneElement(t[i][0], t[i][1], t[i][2]);

	macierz.ShowList();
	std::cout << std::endl;

	std::cout << "Kruskal: " << std::endl;
	macierz.Kruskal();
	std::cout << "Dijkstra: " << std::endl;
	macierz.DijkstraLinear();
}

void ShowGraphAdjacencyLists(int ** t, int topNumber, int edgeNumber)
{
	GraphAdjacencyLists macierz(topNumber, edgeNumber);

	for (int i = 0; i < edgeNumber; i++)
		macierz.ReadOneElement(t[i][0], t[i][1], t[i][2]);

	macierz.ShowList();
}

double KruskalGraphAdjacencyLists(int ** t, int topNumber, int edgeNumber)
{
	GraphAdjacencyLists macierz(topNumber, edgeNumber);
	TimeStoper stoper;
	double returnStoper = 0;
	int wynik = 0;

	for (int i = 0; i < edgeNumber; i++)
		macierz.ReadOneElement(t[i][0], t[i][1], t[i][2]);

	std::cout << "Kruskal listowo: ";
	stoper.StartCounter();
	wynik = macierz.Kruskal();
	returnStoper = stoper.GetCounter();
	std::cout << wynik << "\n";

	return returnStoper;
}

double DijkstrGraphAdjacencyLists(int ** t, int topNumber, int edgeNumber, int start, int end)
{
	GraphAdjacencyLists macierz(topNumber, edgeNumber);
	TimeStoper stoper;
	double returnStoper = -1;

	for (int i = 0; i < edgeNumber; i++)
		macierz.ReadOneElement(t[i][0], t[i][1], t[i][2]);

	std::cout << "Dijkstra listowo: \n";
	stoper.StartCounter();
	macierz.DijkstraLinear(start, end);
	returnStoper = stoper.GetCounter();

	return returnStoper;
}

double PrimaGraphAdjacencyLists(int ** t, int topNumber, int edgeNumber)
{
	GraphAdjacencyLists macierz(topNumber, edgeNumber);
	TimeStoper stoper;
	double returnStoper = 0;

	for (int i = 0; i < edgeNumber; i++)
		macierz.ReadOneElement(t[i][0], t[i][1], t[i][2]);

	std::cout << " Prima listowo: \n";
	stoper.StartCounter();
	macierz.Prima();
	returnStoper = stoper.GetCounter();

	return returnStoper;
}

double BellmanaFordaGraphAdjacencyLists(int ** t, int topNumber, int edgeNumber, int start, int end)
{
	GraphAdjacencyLists macierz(topNumber, edgeNumber);
	TimeStoper stoper;
	double returnStoper = -1;

	for (int i = 0; i < edgeNumber; i++)
		macierz.ReadOneElement(t[i][0], t[i][1], t[i][2]);

	
		std::cout << " Bellman-Ford listowo: \n";
		stoper.StartCounter();
		macierz.BellmanFord();
		returnStoper = stoper.GetCounter();


	return returnStoper;
}

void Test(int **t, int topNumber, int edgeNumber)
{
	int il = 0, q = 0, v = 0;
	double stoperListDijkstraWynik = 0;
	double stoperListKruskalWynik = 0;
	double stoperMatrixDijkstraWynik = 0;
	double stoperMatrixKruskalWynik = 0;

	std::ofstream plik;
	std::string name;
	name = "wyniki"; //nazwa pliku g�sto��_ilo�� wierzcho�k�w

	plik.open((name + ".txt"), std::ios::out);

	std::cout << "Podaj ilosc powtorzen:";
	std::cin >> il;

	std::cout << "Podaj ilosc wierzcholkow:";
	std::cin >> v;

	int tab[4] = { 25,50,75,99 };

	if (plik.good() == true)
	{
		for (int i = 0; i < 4; i++)
		{
			q = tab[i];

			srand(time(NULL));
			int start = 0, end = 0;

			for (int i = 0; i < il; i++)
			{
				start = std::rand() % (topNumber);
				end = std::rand() % (topNumber);
				t = GenerateGraph(q, v); //za ka�dym razem generuje inny wykres

				GraphAdjacencyLists macierz(v, q*v*(v - 1) / 200);
				TimeStoper stoperListKruskal;
				TimeStoper stoperListDijkstra;
				TimeStoper stoperMatrixKruskal;
				TimeStoper stoperMatrixDijkstra;


				for (int i = 0; i < edgeNumber; i++)
					macierz.ReadOneElement(t[i][0], t[i][1], t[i][2]);

				stoperListDijkstra.StartCounter();
				macierz.DijkstraLinearTime(start, end);
				stoperListDijkstraWynik += stoperListDijkstra.GetCounter();

				stoperListKruskal.StartCounter();
				macierz.KruskalTime();
				stoperListKruskalWynik += stoperListKruskal.GetCounter();

				stoperMatrixDijkstra.StartCounter();
				macierz.DijkstraLinearTime(start, end);
				stoperMatrixDijkstraWynik += stoperMatrixDijkstra.GetCounter();

				stoperMatrixKruskal.StartCounter();
				macierz.KruskalTime();
				stoperMatrixKruskalWynik += stoperMatrixKruskal.GetCounter();
			}

			stoperListDijkstraWynik /= il;
			stoperListKruskalWynik /= il;
			stoperMatrixDijkstraWynik /= il;
			stoperMatrixKruskalWynik /= il;

			plik << stoperListDijkstraWynik << "; " << stoperListKruskalWynik << "; " << stoperMatrixDijkstraWynik << "; " << stoperMatrixKruskalWynik << "\n";
		}

		plik.close();
	}
}

void ToFile(int ** t, int edge, int v, int q)
{
	std::ofstream plik;
	std::string name;
	float d = 200 * edge / (v*(v - 1));
	//name = std::to_string(q) + "_" + std::to_string(v); //nazwa pliku g�sto��_ilo�� wierzcho�k�w
	name = std::to_string(v);

	plik.open((name + ".txt"), std::ios::out);

	if (plik.good() == true)
	{
		plik << edge;		//liczba kraw�dzi
		plik << " ";
		plik << v;			//liczba wierzcho�k�w
		plik << "\n";

		for (int i = 0; i < edge; i++)
		{
			plik << t[i][0];		//pocz�tek kraw�dzi
			plik << " ";
			plik << t[i][1];		//koniec kraw�dzi
			plik << " ";
			plik << t[i][2];		//waga
			plik << "\n";
		}
		plik.close();
	}
}

int ** ReadFromFile(std::string name, int q, int v)
{
	std::fstream file;

	name = std::to_string(v);

	file.open(name + ".txt", std::ios::in);

	if (file.good() == true)
	{
		std::string line;
		getline(file, line);//wczytuje rozmiary z pliku: liczbaKraw�dzi liczba wierzcho�k�w
		std::istringstream iss(line);//potrzebne do podzia�u stringa

		int info[2];
		int licz = 0;
		while (iss >> line)//dzieli stringa 
		{
			if (licz < 2)
			{
				info[licz++] = std::stoi(line);
			}
		}

		int edge = (q*v*(v - 1)) / 2;

		if (edge != info[0])
			edge = info[0];
		if (v != info[1])
			v = info[1];
		

		int **t = new int*[edge];
		for (int i = 0; i < edge; i++)
			t[i] = new int[3];

		for (int i = 0; i < edge; i++)
		{
			licz = 0;
			getline(file, line);
			std::istringstream iss(line);

			while (iss >> line)
			{
				t[i][licz++] = std::stoi(line);
			}
		}

		return t;
	}

	return nullptr;
}

int **GenerateGraph(int q, int topNumber)
{
	int edge = (q*topNumber*(topNumber-1)) / 200 ;//obliczam ilo�� kraw�dzi
	int maxWeight = 8;
	bool *isInGraph = new bool[topNumber];

	int **tab = new int *[edge]; //tworz� tablic� il wierzcho�k�w x 3 (pocz�tek kraw�dzi, koniec kraw�dzi, waga)
	for (int i = 0; i < edge; i++)
		tab[i] = new int[3];

	for (int i = 0; i < topNumber; i++) //�aden wierzcho�ek nie jest w grafie
		isInGraph[i] = false;

	srand(time(NULL));

	for (int i = 0; i < edge; i++)
	{
	
		//losuje wierzcho�ki i wage, wierzcho�ki musz� by� r�ne 
		if (i == 0)//pierwsze losowanie
		{
		isTheSame:
			tab[i][0] = std::rand() % (topNumber);
			tab[i][1] = std::rand() % topNumber;

			if (tab[i][0] == tab[i][1])
			{
				if (tab[i][0] != 0)
					tab[i][1]--;
				else
					tab[i][1]++;
			}
			for (int j = 0; j < i; j++) //unikam wielokrotnych kraw�dzi
			{
				if ((tab[i][0] == tab[j][0] && tab[i][1] == tab[j][1]) || (tab[i][0] == tab[j][1] && tab[i][1] == tab[j][0]))
				{
					goto isTheSame;
				}
			}
			tab[i][2] = std::rand() % maxWeight + 1;
			isInGraph[tab[i][0]] = true;
			isInGraph[tab[i][1]] = true;
		}
		else
		{
		isTheSameE:
			isntInGraph:
			tab[i][0] = std::rand() % (topNumber);
			if (isInGraph[tab[i][0]] == false)//wylosowany wierzcho�ek jeden jest w grafie
				goto isntInGraph;
			tab[i][1] = std::rand() % topNumber;

			if (tab[i][0] == tab[i][1])
			{
				if (tab[i][0] != 0)
					tab[i][1]--;
				else
					tab[i][1]++;
			}
			for (int j = 0; j < i; j++) //unikam wielokrotnych kraw�dzi
			{
				if ((tab[i][0] == tab[j][0] && tab[i][1] == tab[j][1]) || (tab[i][0] == tab[j][1] && tab[i][1] == tab[j][0]))
				{
					goto isTheSameE;
				}
			}
			tab[i][2] = std::rand() % maxWeight + 1;
			isInGraph[tab[i][0]] = true;
			isInGraph[tab[i][1]] = true;
		}		
	}

	ToFile(tab, edge, topNumber, q);
	return tab;
}