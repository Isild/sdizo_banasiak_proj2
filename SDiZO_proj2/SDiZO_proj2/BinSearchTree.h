#pragma once
#include "BSTNode.h"
#include <iostream>
#include <string>
#include "List.h"

class BinSearchTree
{
private: 
	BSTNode *root;
	List *list = new List();
	int size = 0;
public:
	void SetRoot(BSTNode *r);
	BSTNode *GetRoot();
	void AddToTree(int s, int e, int w);
	void ShowBST(std::string sp="", std::string sn="", int v=0);
	void Show(BSTNode *tmp, int i=0);
	List *InOrder(BSTNode *tmp);


	BinSearchTree();
	BinSearchTree(BSTNode *r);
	~BinSearchTree();
};

