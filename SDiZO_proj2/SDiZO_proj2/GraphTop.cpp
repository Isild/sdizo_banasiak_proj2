#include "stdafx.h"
#include "GraphTop.h"


GraphTop * GraphTop::GetGraphTopNext()
{
	return next;
}

void GraphTop::SetGraphTopNext(GraphTop * n)
{
	next = n;
}

int GraphTop::GetValue()
{
	return value;
}

void GraphTop::SetValue(int v)
{
	value = v;
}

int GraphTop::GetWeight()
{
	return weight;
}

void GraphTop::SetWeight(int w)
{
	weight = w;
}

GraphTop::GraphTop()
{
}

GraphTop::GraphTop(int v, int w)
{
	value = v;
	weight = w;
	next = nullptr;
}

GraphTop::GraphTop(int v, int w, GraphTop * n)
{
	value = v;
	weight = w;
	next = n;
}

GraphTop::~GraphTop()
{
	value = NULL;
	weight = NULL;
	delete next;
	next = nullptr;
}
