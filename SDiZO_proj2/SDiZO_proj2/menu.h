#pragma once

#include "GraphIncidenceMatrix.h"
#include "GraphAdjacencyLists.h"
#include "GraphTop.h"


void menu();

void TestGraphIncidence(int **t, int topNumber, int edgeNumber); //testowanie Grafu w macierzy incydencji
void ShowGraphIncidence(int **t, int topNumber, int edgeNumber);
double KruskalGraphIncidence(int **t, int topNumber, int edgeNumber);
double DijkstrGraphIncidence(int **t, int topNumber, int edgeNumber, int start, int end);
double PrimaGraphIncidence(int **t, int topNumber, int edgeNumber);
double BellmanaFordaGraphIncidence(int **t, int topNumber, int edgeNumber, int start, int end);

void TestGraphAdjacencyLists(int **t, int topNumber, int edgeNumber); //testowanie Grafu w li�cie s�siedztwa
void ShowGraphAdjacencyLists(int **t, int topNumber, int edgeNumber);
double KruskalGraphAdjacencyLists(int **t, int topNumber, int edgeNumber);
double DijkstrGraphAdjacencyLists(int **t, int topNumber, int edgeNumber, int start, int end);
double PrimaGraphAdjacencyLists(int **t, int topNumber, int edgeNumber);
double BellmanaFordaGraphAdjacencyLists(int **t, int topNumber, int edgeNumber, int start, int end);

void Test(int **t, int topNumber, int edgeNumber);

void ToFile(int **t, int edge, int v, int q);
int **ReadFromFile(std::string name, int q, int v);

int **GenerateGraph(int q, int topNumber);//g�sto��, ilo�� wierzcho�k�w